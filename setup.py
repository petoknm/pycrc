from setuptools import setup, find_packages

setup(
    name="pycrc",
    packages=find_packages(),
    python_requires=">=3.9",
    install_requires=["numpy"],
    extras_require={
        "numba": ["numba"],
        "dev": ["numba", "pytest", "pytest-benchmark"],
    },
)
