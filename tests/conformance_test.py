import pytest
from pycrc import (
    Algorithm,
    BitwiseDigestor,
    LookupTableDigestor,
    Codeword,
    HexadecimalCodeword,
)


def for_each_algorithm(algs: list[Algorithm]):
    return pytest.mark.parametrize("alg", algs, ids=[alg.name for alg in algs])


def for_each_algorithm_codeword(algs: list[Algorithm], only_hex: bool = False):
    inputs = [(alg, Codeword.parse(cw)) for alg in algs for cw in alg.codewords]
    if only_hex:
        inputs = [
            (alg, cw) for alg, cw in inputs if isinstance(cw, HexadecimalCodeword)
        ]
    return pytest.mark.parametrize(
        "alg,cw",
        inputs,
        ids=[f"{alg.name}_{cw.name}" for alg, cw in inputs],
    )


all_algs = Algorithm.load_all()
pow2_algs = [alg for alg in all_algs if alg.width in [8, 16, 32, 64, 128]]

check_data = b"123456789"


@for_each_algorithm(all_algs)
def test_bitwise_check(alg: Algorithm):
    assert BitwiseDigestor(alg).digest(check_data) == alg.check


@for_each_algorithm(pow2_algs)
def test_lookup_check(alg: Algorithm):
    assert LookupTableDigestor(alg).digest(check_data) == alg.check


@for_each_algorithm_codeword(all_algs)
def test_bitwise_codeword(alg: Algorithm, cw: Codeword):
    result = cw.digest_using(BitwiseDigestor(alg))
    assert result ^ alg.xorout == alg.residue


@for_each_algorithm_codeword(pow2_algs, only_hex=True)
def test_lookup_codeword(alg: Algorithm, cw: Codeword):
    result = cw.digest_using(LookupTableDigestor(alg))
    assert result ^ alg.xorout == alg.residue
