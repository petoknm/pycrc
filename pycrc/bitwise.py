from .digestor import Digestor
from .util import byte_iter


class BitwiseDigestor(Digestor):
    def update(self, data: bytes):
        for byte in byte_iter(data, self.alg.refin):
            for pos in reversed(range(8)):
                self.update_bit((byte >> pos) & 1)

    def update_bit(self, bit: int):
        self.crc ^= bit * self.alg.top_bit_mask
        if self.crc & self.alg.top_bit_mask:
            self.crc = (self.crc << 1) ^ self.alg.poly
        else:
            self.crc = self.crc << 1
        self.crc &= self.alg.mask
