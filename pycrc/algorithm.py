from dataclasses import dataclass
from typing import Type
from functools import cache
from importlib.resources import open_text
import json


@dataclass(frozen=True)
class Algorithm:
    name: str
    width: int
    poly: int
    init: int
    refin: bool
    refout: bool
    xorout: int
    check: int
    residue: int
    codewords: list[str]

    @property
    def mask(self) -> int:
        return (1 << self.width) - 1

    @property
    def top_bit_mask(self) -> int:
        return 1 << (self.width - 1)

    @property
    def width_bytes(self) -> int:
        return self.width // 8

    @staticmethod
    def from_json(data: dict) -> Type["Algorithm"]:
        return Algorithm(
            name=data["name"],
            width=data["width"],
            poly=int(data["poly"], base=0),
            init=int(data["init"], base=0),
            refin=data["refin"],
            refout=data["refout"],
            xorout=int(data["xorout"], base=0),
            check=int(data["check"], base=0),
            residue=int(data["residue"], base=0),
            codewords=data["codewords"],
        )

    @staticmethod
    @cache
    def load_all() -> list[Type["Algorithm"]]:
        with open_text(__package__, "algorithms.json") as fp:
            return [Algorithm.from_json(alg) for alg in json.load(fp)]
