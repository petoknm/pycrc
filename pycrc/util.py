from collections.abc import Iterable


def reflect(n: int, width: int) -> int:
    res = 0
    for _ in range(width):
        res = (res << 1) + (n & 1)
        n >>= 1
    return res


def byte_iter(data: bytes, refin: bool) -> Iterable[int]:
    for byte in data:
        yield reflect(byte, 8) if refin else byte
