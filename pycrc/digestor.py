from abc import ABC, abstractmethod
from .algorithm import Algorithm
from .util import reflect


class Digestor(ABC):
    def __init__(self, alg: Algorithm) -> None:
        self.alg, self.crc = alg, alg.init

    @abstractmethod
    def update(self, data: bytes):
        pass

    def finalize(self) -> int:
        ref = reflect(self.crc, self.alg.width) if self.alg.refout else self.crc
        return ref ^ self.alg.xorout

    def digest(self, data: bytes) -> int:
        self.update(data)
        return self.finalize()
