from .algorithm import Algorithm
from .digestor import Digestor
from .util import byte_iter


class LookupTableDigestor(Digestor):
    def __init__(self, alg: Algorithm):
        super().__init__(alg)
        assert alg.width in [8, 16, 32, 64, 128]
        self.table = [self._table_item(alg, i) for i in range(256)]

    @staticmethod
    def _table_item(alg: Algorithm, index: int) -> int:
        res = index
        for _ in range(alg.width):
            res = (res << 1) ^ (alg.poly if res & alg.top_bit_mask else 0)
        return res & alg.mask

    def update(self, data: bytes):
        for byte in byte_iter(data, self.alg.refin):
            top = self.crc >> (self.alg.width - 8)
            self.crc = (self.crc << 8) ^ self.table[top ^ byte]
            self.crc &= self.alg.mask
