from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Type
from .digestor import Digestor
from .bitwise import BitwiseDigestor


@dataclass
class Codeword(ABC):
    name: str

    @abstractmethod
    def digest_using(self, digestor: Digestor) -> int:
        pass

    @staticmethod
    def parse(cw: str) -> Type["Codeword"]:
        if cw.startswith("0x"):
            return HexadecimalCodeword(cw, bytes.fromhex(cw[2:]))
        if cw.startswith("0b"):
            return BinaryCodeword(cw, [bit == "1" for bit in cw[2:]])
        if cw.startswith("0i"):
            return IIICodeword(cw, bytes.fromhex(cw[2:]))


@dataclass
class BinaryCodeword(Codeword):
    data: list[bool]

    def digest_using(self, digestor: Digestor) -> int:
        assert isinstance(digestor, BitwiseDigestor)
        for bit in self.data:
            digestor.update_bit(bit)
        return digestor.finalize()


@dataclass
class HexadecimalCodeword(Codeword):
    data: bytes

    def digest_using(self, digestor: Digestor) -> int:
        return digestor.digest(self.data)


class IIICodeword(HexadecimalCodeword):
    def __init__(self, name: str, suffix: bytes):
        data = bytes([(i + i * i) % 256 for i in range(1999)]) + suffix
        super().__init__(name, data)
